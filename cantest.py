from collections import OrderedDict
from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog
from tkinter import messagebox
from local_checkboxtreeview import CheckboxTreeview # pip install ttkwidgets
from copy import *
from pprint import pprint

import json
import xml.etree.ElementTree as ET
import xml.dom.minidom
import pyexcel # pip install pyexcel

#### Core:

def load_json_types(filename):
    with open(filename, 'r') as infile:
        data_type_raw = json.load(infile, object_pairs_hook=OrderedDict)

    #data_type_size = {'float': 4, 'double': 8, 'int': 4, 'uint': 4, 'bool': 1, }
    data_type_size = {'float': 4, 'double': 4, 'int': 1, 'uint': 1, 'bool': 1, }

    try:
        # add to type name also type size
        data_type_raw = data_type_raw['TypeInfos']
        data_type = dict()
        for d in data_type_raw:
            for k,v in d['Propertys'].items():
                d['Propertys'][k] = (v, data_type_size[v])

            data_type[d['TypeName']] = d['Propertys']

        return data_type
    except Exception as e:
        print('invalid json format. Error: '+str(e))
        raise

def load_table(filename):
    book = pyexcel.get_book(file_name=filename, delimiter=';', encoding='utf-8-sig')
    d = book.sheet_by_index(0)
    del d.row[0] # remove first row (headers)
    return d

def process_data(data_type, tags_table):
    # combine Table and Json data
    data_result_table = []
    data_result_dict = {}
    table_offset = 0
    # enum table
    ##for d_path, d_type, d_addr in tags_table.rows():
    for d_path, d_type, d_addr in tags_table:
        if d_type in data_type:
            sub_dict = {}
            # enum var list (from json) for every record
            for value_name, value in data_type[d_type].items():
                sub_dict.update({value_name: table_offset})
                data_result_table.append([d_path + '.' + value_name, table_offset])
                table_offset += value[1]
            data_result_dict.update({d_path: sub_dict})
        else:
            print('error: invalid type({0}) in csv file (skip record).'.format(d_type))
    return (data_result_table, data_result_dict)

def create_table_xml(data):
    xml_tree = ET.Element('root')
    for tag, address in data:
        item = ET.SubElement(xml_tree, 'item', {'Binding': 'Introduced'})
        ET.SubElement(item, 'node-path').text = str(tag)
        ET.SubElement(item, 'address').text = str(address)
    return xml_tree

def save_xml_result(filename, data_result_table):
    xml_tree = create_table_xml(data_result_table)
    xml_str_pretty = xml.dom.minidom.parseString(ET.tostring(xml_tree)).toprettyxml()
    with open(filename, "w") as fh:
        fh.write(xml_str_pretty)

#### Old:

def console_main():
    data_type = load_json_types('TypeInfos.json')
    tags_table = load_table('Input.csv')
    data_result_table, _ = process_data(data_type, tags_table)
    save_xml_result('result2.xml', data_result_table)

#### TreeView Utils:

def TV_get_item_values(tree: Treeview, item):
    return [tree.item(item, 'text')] + list(tree.item(item, 'values'))

def TV_dict_to_tree(tree: Treeview, parent, item_key, item_value, proc_item_name, proc_item = None):
    if isinstance(item_value, (dict, list)):
        new_parent = tree.insert(parent, END, None, text=item_key)
        if not proc_item is None:
            proc_item(tree, new_parent, item_value, False)
        for key, value in item_value.items():
            # RECURSION
            TV_dict_to_tree(tree, new_parent, key, value, proc_item_name, proc_item)
    else:
        itmp = tree.insert(parent, END, None, text=proc_item_name(item_key, item_value))
        if not proc_item is None:
            proc_item(tree, itmp, item_value, True)

def TV_set_head(tree, headings):
    ''' ( (text, width), (text, width), ... ) '''

    tree["displaycolumns"] = '#all'
    if isinstance(headings, (list, tuple, dict)):
        tree["columns"] = tuple(range(1, len(headings)))
        for i, head in enumerate(headings):
            i = '#0' if i==0 else i
            tree.heading(i, text=head[0], anchor=W)
            # https://docs.python.org/3/library/tkinter.ttk.html#tkinter.ttk.Treeview.column
            tree.column(i, anchor=W, width=head[1])
    else:
        tree["columns"] = (0)
        tree.heading(0, text=headings, anchor=W)

#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####

class Table(Frame):
    ''' Table widget (based on Treeview)
        # URL: https://docs.python.org/3/library/tkinter.ttk.html#treeview
    '''

    def __init__(self, parent=None, use_check_box: bool = False):
        super().__init__(parent)

        self.use_check_box = use_check_box
        if use_check_box:
            self.table = CheckboxTreeview(self)
        else:
            self.table = Treeview(self)

        self.scrolltable = Scrollbar(self, command=self.table.yview)
        self.table.configure(yscrollcommand=self.scrolltable.set)
        self.scrolltable.pack(side=RIGHT, fill=Y)
        self.table.pack(expand=YES, fill=BOTH)

    def clear(self):
        self.table.delete(*self.table.get_children())

    def set_data(self, rows):
        self.clear()
        for i, row in enumerate(rows):
            self.table.insert('', END, iid=i, text=row[0], values=row[1:])

    def set_data_list(self, rows: list):
        self.clear()
        for i, row in enumerate(rows):
            self.table.insert('', END, iid=i, text=row)

    def checkbox_set_all(self, enable):
        if isinstance(enable, bool):
            enable = 'checked' if enable else 'false'
        if self.use_check_box:
            for i in self.table.get_children():
                self.table.change_state(i, enable)

#### #### #### #### #### #### #### #### #### #### #### #### #### #### #### ####



class Application(Frame):
    ''' GUI application class  '''

    def __init__(self, master=None):
        super().__init__(master)
        self.data_type = [] # info about types (from JSON)
        self.tags_table = [] # tags table (from CSV)
        self.data_result_table = [] # combine result table
        self.master = master
        self.pack(fill=BOTH, expand=True)
        self.create_widgets()

    # make check/uncheck on press space button
    # todo: make universal solution and PR to https://github.com/TkinterEP/ttkwidgets
    #  self.table_list.table.bind("<Key-space>", self.space_press, True)
    def space_press(self, event):
        self = self.table_list.table
        for item in self.selection():
            if self.tag_has("unchecked", item) or self.tag_has("tristate", item):
                self._check_ancestor(item)
                self._check_descendant(item)
            else:
                self._uncheck_descendant(item)
                self._uncheck_ancestor(item)

    def create_widgets(self):
        # anchor:
        #  N
        # W E
        #  S

        #self.lb_last_op = Label(self, anchor=NW, justify=LEFT)
        #self.lb_last_op["text"] = "Result of the last operation:"
        #self.lb_last_op.pack(side="top")
        #self.lb_last_op.pack(fill=X)

        fr1 = Frame(self)
        self.types_list = Treeview(fr1)
        TV_set_head(self.types_list, [('Json tree', 350)] )
        scrolltable = Scrollbar(fr1, command=self.types_list.yview)
        self.types_list.configure(yscrollcommand=scrolltable.set)
        scrolltable.pack(side=RIGHT, fill=Y)
        self.types_list.pack(anchor=CENTER, fill=BOTH, expand=YES)

        self.table_list = Table(self, use_check_box=True)
        TV_set_head(self.table_list.table, [('Tag', 300), ('Type', 70), ('Address', 50)] )
        # key code: http://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm
        self.table_list.table.bind("<Key-space>", self.space_press, True)

        fr2 = Frame(self)
        self.result_list = Treeview(fr2)
        TV_set_head(self.result_list, [('Type', 300), ('Variable', 70)] )
        scrolltable2 = Scrollbar(fr2, command=self.result_list.yview)
        self.result_list.configure(yscrollcommand=scrolltable2.set)
        scrolltable2.pack(side=RIGHT, fill=Y)
        self.result_list.pack(anchor=CENTER, fill=BOTH, expand=YES)

        fr1.pack(side=LEFT, fill=Y)
        fr2.pack(side=RIGHT, fill=Y)
        self.table_list.pack(anchor=W, fill=BOTH, expand=YES)

        #self.types_list.grid(row=0, column=0)
        #scrolltable.grid(row=0, column=1, fill=Y)
        #self.table_list.grid(row=0, column=2)
        #self.result_list.grid(row=0, column=3)
        #scrolltable2.grid(row=0, column=4, fill=Y)

        #self.lb_info = Label(self, anchor=SW, justify=LEFT)
        #self.lb_info["text"] = ""
        #self.lb_info.pack(fill=X)

        # create a toplevel menu
        self.menubar = Menu(self)
        self.menubar.add_command(label="Load JSON", command=self.load_json); self.m1 = 1
        self.menubar.add_command(label="Load CSV", command=self.load_csv); self.m2 = 2
        self.menubar.add_command(label="Reload data", command=self.reload_data, state=DISABLED); self.m3 = 3
        self.menubar.add_command(label="Process", command=self.data_process, state=DISABLED); self.m4 = 4
        self.menubar.add_command(label="Save XML", command=self.save_xml, state=DISABLED); self.m5 = 5
        self.menubar.add_command(label="Exit", command=self.master.destroy)
        #self.menubar.add_command(label="Test", command=self.test);
        self.master.config(menu=self.menubar)

    def test(self):
        print(self.types_list.get_children())
        print(self.table_list.table.get_children())
        print(self.table_list.table.get_children('1'))
        print(self.table_list.table.item('1'))
        #self.types_list.enable_checkbox()
        pass

    def menu_update(self):
        if self.data_type != []:
            self.menubar.entryconfig(self.m2, state=NORMAL)
        if self.data_type != [] and self.tags_table != []:
            self.menubar.entryconfig(self.m3, state=NORMAL)
            self.menubar.entryconfig(self.m4, state=NORMAL)
        if self.data_result_table != []:
            self.menubar.entryconfig(self.m5, state=NORMAL)

    def reload_data(self):
        self.data_type = []
        self.tags_table = []
        self.menu_update()
        try:
            self.data_type = load_json_types(self.json_filename)
            self.tags_table = load_table(self.csv_filename)
            self.menu_update()
        except Exception as e:
            messagebox.showerror('Error', str(e))

    def load_json(self):
        self.json_filename = filedialog.askopenfilename(initialdir=".", title="Load JSON file with Data Type list",
                                                   filetypes=(("JSON files", "*.json"), ("all files", "*.*")))

        if self.json_filename != '':
            try:
                self.data_type = load_json_types(self.json_filename)

                TV_dict_to_tree(self.types_list, self.types_list.get_children(), 'Root', self.data_type,
                                lambda n, v: '{0}: {1} ({2})'.format(n, v[0], v[1]))
                self.types_list.item(self.types_list.get_children(), open=True)
                self.menu_update()
            except Exception as e:
                messagebox.showerror('Error', 'invalid json format. Error: ' + str(e))

    def load_csv(self):
        self.csv_filename = filedialog.askopenfilename(initialdir=".", title="Load CSV file with data types_list",
                                                   filetypes=(("CSV files", "*.csv"), ("all files", "*.*")))

        if self.csv_filename != '':
            try:
                self.tags_table = load_table(self.csv_filename)
                self.table_list.set_data(self.tags_table)
                self.table_list.checkbox_set_all(enable=True)
                self.menu_update()
            except Exception as e:
                messagebox.showerror('Error', str(e))

    def data_process(self):
        # filter checked records
        tags_table = []
        for i in self.table_list.table.get_children():
            if 'checked' in self.table_list.table.item(i, 'tags'):
                tags_table.append(TV_get_item_values(self.table_list.table, i))

        # process records
        self.data_result_table, result_dict = process_data(self.data_type, tags_table)

        def proc_item(tree, iid, value, is_item):
            tree.item(iid, open=True)
            if is_item:
                tree.item(iid, open=True, values=[value])

        # show result
        TV_dict_to_tree(self.result_list, self.result_list.get_children(), 'Root', result_dict,
                    lambda n, v: n, proc_item)

        self.menu_update()


    def save_xml(self):
        self.xml_filename = filedialog.asksaveasfilename(initialdir=".", title="Save result XML file",
                                                   filetypes=(("XML files", "*.xml"), ("all files", "*.*")))

        if self.xml_filename != '':
            save_xml_result(self.xml_filename, self.data_result_table)



def main():
    gui = Tk()
    app = Application(master=gui)
    app.mainloop()

if __name__ == '__main__':
    main()



'''
future feature:

console mode.

import docopt # pip install docopt

usage = """
Usage: cantest.py [--datatypes=FILE] [--tagstable=FILE] [--result=FILE]

Options:
    --datatypes=FILE   File in JSON format [default: TypeInfos.json]
    --tagstable=FILE   File in CSV format  [default: Input.csv]
    --result=FILE      Output file in XML format
    --gui              Enable GUI mode (default)
"""

'''